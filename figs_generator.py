"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

With this script you can generate all the comparative figures of the paper.

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
from cohorts_definition import load_cohorts
from utilities.figs_helper import  load_inspection_data_set, master_plots_DTMCs,load_all_data, load_cases
import numpy as np
#%% Load dependencies:
# Directory where the DTMCs are located (obtained with the script training.oy):
dir_DTMCs_location = 'cohort_analysis/'
# Path to save the figures:
save_figures_path  = 'figures/'
#%% Load information:    
# Load cohorts:
analysis = load_cohorts()
# Load inspection data set:
ins_data = load_inspection_data_set('data/',
                                    files = {'covariates',
                                             'inspection'
                                             }
                                    )
#%% Get the data to plot
s = 3 # Delta_t (interval size) 
t = np.arange(0,127,s) # Time-horizon
# Load the data from all the case studies of interest:
print('... importing all the necessary data from the cohorts ...')
#%% Plots comparing cohorts and expectations: 
c = load_cases('comparing_cohorts')
list_cases = list(set([item for sublist in c for item in sublist]))
data = load_all_data(list_cases,dir_DTMCs_location,t,s,analysis,ins_data)
# Compare cohorts:
print('... creating figures comparing cohorts ...')
master_plots_DTMCs(c,analysis,data,save_figures_path,states=[1,2,3,4,5],save_plot = 1,plot_type='comparing_cohorts')

# Compare expectations:
print('... creating figures comparing expectations ...')
master_plots_DTMCs(c,analysis,data,save_figures_path,states=[1],save_plot = 1,plot_type='comparing_expectations')
#%% Plots comparing chains and DTMC parameters: 
c = load_cases('comparing_chains')
list_cases = list(set([item for sublist in c for item in sublist]))
data = load_all_data(list_cases,dir_DTMCs_location,t,s,analysis,ins_data)

# Compare chains:
print('... creating figures comparing chains ...')
master_plots_DTMCs(c,analysis,data,save_figures_path,states=[1,2,3,4,5],save_plot = 1,plot_type='comparing_chains')  

# Plot distributions of DTMCs parameters
print('... creating figures distributions of DTMCs parameters ...')
master_plots_DTMCs(c,analysis,data,save_figures_path,states=[1],save_plot = 1,plot_type='comparing_transition_probabilities')  
#%%
print('... generation of figures, completed! ...')