"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

Cohorts are defined in this script.

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
import pickle

def load_cohorts(cohort_ = []):
    
    df1 = pickle.load(open('data/covariates',"rb"))
    #%% Focus on the following damage codes:
    codes  = [['BBF'],# Infiltration
             ['BAF'],#  Surface damage
             ['BAB']]#  Crack
    #%% We define two types of discrete-time Markov chain:
    chains = ['Multi','Single']
    #%%
    cohorts = {}
    #%% Cohort CMW (Material: Concrete & Content: Mixed and Waste):
    cohorts["cohort_CMW"] = {
    "idx"      : (((df1['content']=='MixedSewer') | (df1['content']=='WasteSewer')) & (df1['material']=='Concrete'))
    }
    #%% Cohort CR (Material: Concrete & Content: Rainwater):
    cohorts["cohort_CR"] = {
    "idx"      : ((df1['content']=='RainSewer')  & (df1['material']=='Concrete'))
    }
    #%% Cohort PMW (Material PVC & Content: Mixed and Waste):
    cohorts["cohort_PMW"] = {
    "idx"      : (((df1['content']=='MixedSewer') | (df1['content']=='WasteSewer')) & (df1['material']=='PVC'))
    }
    #%% Cohort PR (Material PVC & Content: Rainwater):
    cohorts["cohort_PR"] = {
    "idx"      : ((df1['content']=='RainSewer')  & (df1['material']=='PVC'))
    }
    #%% Cohort CdL (Material: Concrete & Diameter < 500 cm):
    cohorts["cohort_CdL"] = {
    "idx"      : ((df1['material']=='Concrete')  & (df1['width']<500))
    }
    #%%  Cohort CdG (Material: Concrete & Diameter >= 500 cm):
    cohorts["cohort_CdG"] = {
    "idx"      : ((df1['material']=='Concrete')  & (df1['width']>=500))
    }
    #%% Create the analysis:
    analysis = {}
    for i in codes:
        for j in chains:
            for k in cohorts.keys():
                c = ''
                for z in i:
                    c = c + '_' + z
                key = k + '_chain_' + j + '_damagecode' + c
                analysis[key] = {"idx": cohorts[k]["idx"],
                                 "codes": i,
                                 "topology": j,
                                 'key': key,
                                 'cohort_name':k,
                                 'cohort_chain':j}
    return analysis