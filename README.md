# Degradation modeling of sewer pipes via discrete-time Markov chains

This repository contain the scripts, data and comparative figures associated to the paper: Deterioration modeling of sewer pipes via discrete-time Markov chains: A large-scale case study in the Netherlands.

## Repository overview:

### Folders:

- cohort_analysis: contains the trained discrete-time Markov chains for different cohorts and damage classes.
- data: contains csv files of the covariates and inspection data of the case study.
- figures: contains all the comparative figures shown in the paper.
- utilities: necessary additional algorithms.

### Scripts:

- `cohorts_definition.py`: define the cohorts used in the paper.
- `figs_generator.py`: file to generate all the figures in the paper.
- `statistics.py`: file that summarize all the information presented in the paper.
- `training.py`: calibrate the DTMCs based on the defined cohorts and the inspection data.


## Downloading sources
You can clone the repository via the following command:

`$ git clone https://gitlab.utwente.nl/fmt/degradation-models/dtmc_sewer_pipes.git`


## Dependencies

Python 3

## Additional packages

- [numpy](https://numpy.org/install/)
- [pickle](https://pypi.org/project/pickle5/)
- [pandas](https://pypi.org/project/pandas/)
- [sklearn](https://scikit-learn.org/stable/install.html)
- [scipy](https://pypi.org/project/scipy/)

## Notes

This research has been partially funded by Dutch Research Council (NWO) under the grant PrimaVera (https://primavera-project.com) number NWA.1160.18.238.

## Contact

[Lisandro Jimenez](https://people.utwente.nl/l.jimenezroa) <br>
l.jimenezroa@utwente.nl <br>
PhD candidate in Computer Science <br>
Formal Methods & Tools (FMT) group <br>
Faculty of Electrical Engineering, Mathematics and Computer Science (EEMCS) <br>
University of Twente, The Netherlands <br>
