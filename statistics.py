"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

Computing the information presented in the paper.

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
import pickle
from utilities.helper import table_probabilities_states_over_time
from cohorts_definition import load_cohorts
import numpy as np
import pandas as pd
#%%
def statistics_covariates(df,field):
    l = list(df[field].unique())
    t = []
    for i in l:
        t.append(sum(df[field]==i))
    t = list(np.array(t)/df.shape[0]*100)
    d = pd.DataFrame({field:l,
                      'count':t})
    return d.sort_values(by=['count'], ascending=False)

def getTransitionPmatrix(d):
    P = []
    for i in d.keys():
        P.append(d[0]['mc_models'][0]['P'])
    P = np.array(P)
    P_mean = np.mean(P,axis=0)
    return P_mean
#%% Load data:
covariates  = pickle.load(open('data/covariates',"rb"))
inspection  = pickle.load(open('data/inspection',"rb"))
#%%
print('Number of pipes in the dataset: ' + str(len(covariates.pipe_id.unique())))
print('Number of pipes inspected: ' + str(len(inspection[('pipe_id','')].unique())))
#%% Information in Table 1:
inspected_pipes = list(set(list(covariates.pipe_id.unique())) & set(list(inspection[('pipe_id','')].unique())) )
df = covariates[covariates['pipe_id'].isin(inspected_pipes)]

# Fraction of pipes that have Material: Concrete & Content: Mixed and Waste:
idx = (((df['content']=='MixedSewer') | (df['content']=='WasteSewer')) & (df['material']=='Concrete') )
print('Fraction of pipes that have Material: Concrete & Content: Mixed and Waste: ' + str(round(100*sum(idx)/df.shape[0],2)) + '%')

# Fraction of pipes that have Material: Concrete & Content: Rainwater:
idx = ((df['content']=='RainSewer')  & (df['material']=='Concrete'))
print('Fraction of pipes that have Material: Concrete & Content: Rainwater: ' + str(round(100*sum(idx)/df.shape[0],2)) + '%')

# Fraction of pipes that have Material: PVC & Content: Mixed and Waste:
idx = (((df['content']=='MixedSewer') | (df['content']=='WasteSewer')) & (df['material']=='PVC'))
print('Fraction of pipes that have Material: PVC & Content: Mixed and Waste: ' + str(round(100*sum(idx)/df.shape[0],2)) + '%')

# Fraction of pipes that have Material: PVC & Content: Rainwater:
idx = ((df['content']=='RainSewer')  & (df['material']=='PVC'))
print('Fraction of pipes that have Material: PVC & Content: Rainwater: ' + str(round(100*sum(idx)/df.shape[0],2)) + '%')

# Fraction of pipes that have Width<500mm & Material: Concrete:
idx = ((df['material']=='Concrete')  & (df['width']<500))
print('Fraction of pipes that have Width<500mm & Material: Concrete: ' + str(round(100*sum(idx)/df.shape[0],2)) + '%')

# Fraction of pipes that have Width<500mm & Material: Concrete:
idx = ((df['material']=='Concrete')  & (df['width']>=500))
print('Fraction of pipes that have Width>=500mm & Material: Concrete: ' + str(round(100*sum(idx)/df.shape[0],2)) + '%')
#%%  Information in Table 2:
case = 'cohort_CMW_chain_Single_damagecode_BAF'
analysis = load_cohorts()
d = inspection[inspection['pipe_id'].isin(covariates['pipe_id'][analysis[case]['idx']])]
discretized_table = table_probabilities_states_over_time(d,t=3,damage_codes=analysis[case]['codes'],damage_class=[1,2,3,4,5],flag = 'most_critical')
table2 = discretized_table.iloc[[0,16,25]]
print(table2)
#%% Information in Section 4.1 (Case study):
print('The length of the sewer pipe network is: ' + str(round(sum(covariates.length)/1000,0)) + ' kms' )
# Most of the pipes were build from the 1950 onwards, as can be seen in the histogram below:
covariates.construction_year.hist()
# Number of pipes per material:
material = statistics_covariates(df,'material')
print(' ')
print('Number of pipes per material:')
print(material)
# Number of pipes per shape:
shape = statistics_covariates(df,'shape')
print(' ')
print('Number of pipes per shape:')
print(shape)
# Number of pipes per function:
function = statistics_covariates(df,'function')
print(' ')
print('Number of pipes per function:')
print(function)
# Number of pipes per content:
content = statistics_covariates(df,'content')
print(' ')
print('Number of pipes per content:')
print(content)
#%% About the damages:
print(' ')
print('Proportion of damages in dataset:')
print('Damage Infiltration (BBF): '+str(round(100*sum(inspection.BBF.any(axis=1))/inspection.shape[0],0))+'%')
print('Damage Surface damage (BAF): '+str(round(100*sum(inspection.BAF.any(axis=1))/inspection.shape[0],0))+'%')    
print('Damage Crack (BAB): '+str(round(100*sum(inspection.BAB.any(axis=1))/inspection.shape[0],0))+'%')
#%% Computing the transition probabilities of DTMCs in Figure 1.
print('Parameters in DTMC (Fig. 1):')
multi  = getTransitionPmatrix(pickle.load(open('cohort_analysis/cohort_CdG_chain_Multi_damagecode_BAF',"rb")))
print('Chain Multi:')
print(np.round(multi,4))
single = getTransitionPmatrix(pickle.load(open('cohort_analysis/cohort_CdG_chain_Single_damagecode_BAF',"rb")))
print('Chain Single:')
print(np.round(single,4))


