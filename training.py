"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

With this script train the DTMCs for given cohorts.

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
import pickle
import numpy as np
from utilities.dtmc import dtmc
from utilities.helper import table_probabilities_states_over_time
from cohorts_definition import load_cohorts
from os import listdir
from os.path import isfile, join
from sklearn.utils import resample
import time
import os 

#%% Input data:
# Place to save the trained DTMCs:
save_cohort_results = 'cohort_analysis/'
#  Delta_t (window size for discretized table).
interval_years = 3
#%% Load the dataset
# Load covariates:
covariates  = pickle.load(open('data/covariates',"rb"))
# Load inspection data:
inspection  = pickle.load(open('data/inspection',"rb"))
#%% Load the cohorts:
analysis = load_cohorts()
# Leave only the analysis that have not yet been done.
# Check first if the folder exist:
if not os.path.exists(save_cohort_results):
    os.makedirs(save_cohort_results)
onlyfiles = [f for f in listdir(save_cohort_results) if isfile(join(save_cohort_results, f))]
#%% Calibrate Markov model:
# Training parameters:
training_parameters = {'n_iterations_bootstrap':1000,   # bootstrap iterations
                       'n_repetitions_per_iteration': 1, # n_repetitions_per_iteration
                       'proportion':0.5,
                       }
#%%
for cases_ in list(set(analysis.keys())-set(onlyfiles)):
    
    print('... Analysing:' + cases_)
    stats = list()
    Pt = {}
    if np.sum(analysis[cases_]['idx']+0)>0:
        # Data associated to a cohort:
        d = inspection[inspection['pipe_id'].isin(covariates['pipe_id'][analysis[cases_]['idx']])]
        for n_iterations in range(training_parameters['n_iterations_bootstrap']):
     
            # prepare train and test sets
            Pt[n_iterations] = {}
            
            # Split data set into training and testing (half sample bootstrap):
            train = resample(d, n_samples=int(d.shape[0]*training_parameters['proportion']))          
            test  = d.merge(train, how = 'outer' ,indicator=True).loc[lambda x : x['_merge']=='left_only']
            
            # Discretized table:
            t1 = time.time()
            discretized_table = table_probabilities_states_over_time(train,interval_years,damage_codes=analysis[cases_]['codes'],damage_class=[1,2,3,4,5],flag = 'most_critical')
            t1 = time.time() - t1 # Time to converge
            
            # Train the DTMC:
            dataset = {'proportions_per_class': np.array(discretized_table[[1,2,3,4,5]]),
                       'counts': np.array(discretized_table['count']),
                       'discretized_n_step_vector': np.array(discretized_table.index),
                       }
            
            models , errors, times = dtmc(dataset = dataset, topology = analysis[cases_]['topology'],n_repetitions=training_parameters['n_repetitions_per_iteration'])

            # About the DTMCs:
            Pt[n_iterations]['mc_models']                   = models
            Pt[n_iterations]['DTMC_calibration_time']       = times
            Pt[n_iterations]['discretization_time']         = t1
            Pt[n_iterations]['errors']                      = errors
            Pt[n_iterations]['Delta_t']                     = interval_years
            Pt[n_iterations]['data_used']                   = sum(analysis[cases_]['idx'])/len(analysis[cases_]['idx'])
            Pt[n_iterations]['total_pipes']                 = len(analysis[cases_]['idx'])
            Pt[n_iterations]['n_iterations_bootstrap']      = training_parameters['n_iterations_bootstrap']
            Pt[n_iterations]['n_repetitions_per_iteration'] = training_parameters['n_repetitions_per_iteration']

        # Save Markov model results:
        results = Pt
        pickle.dump(results,open(save_cohort_results + cases_,"wb"))
    else:
        print('The cohort is empty ...')
print(' ... Analysing ended ... ')