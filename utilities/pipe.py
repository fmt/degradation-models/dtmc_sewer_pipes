"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
from copy import deepcopy
import pandas as pd
import numpy as np
from datetime import datetime
import itertools
from varnames import varnames
#%%
class Pipe:
    '''
     Representation of a pipe object
    '''
    
    def __init__(self, data , city , varname_id = 'id' , pipe_location = [], inpsection_dates = []):
        """
        Constructor.
        :param data: inspection data in the data base
        :param name: Name
        """
        colnames_database = list(varnames(city)['pipe_properties'].values())
        colnames_program  = list(varnames(city)['pipe_properties'].keys())
        
        temp1 = data[colnames_database]
        if sum(temp1.nunique()) == len(colnames_database):
            # The information for the pipe is consistent (not different properties)
            for i,j in zip(colnames_program,colnames_database):
                exec("self." + i + " = temp1[j].unique()[0] " )
            
            if pipe_location is not []:    
                # Add nodes location information:
                x1 = pipe_location[0]
                x2 = pipe_location[1]
                # Add the nodes:
                self.start_node_id    = x2['start_node_id'].values[0]
                self.end_node_id      = x2['end_node_id'].values[0]
                self.coor             = x1[np.any([x1['node_id']==self.start_node_id,x1['node_id']==self.end_node_id],axis=0)]
                self.coor.index       = [0,1] # Reset the indexes

            # Keep data from inspections:
            data = data[list(varnames(city)['inspection_properties'].values())]
            # Add inspection date
            for inspection in inpsection_dates['id'].tolist():
                rowIndex = data['sewer_inspection_id'].isin([inspection]) 
                
                year   = np.array(inpsection_dates[inpsection_dates['id'] == inspection]['datetime'])[0][0:4]
                month  = np.array(inpsection_dates[inpsection_dates['id'] == inspection]['datetime'])[0][5:7]
                day    = np.array(inpsection_dates[inpsection_dates['id'] == inspection]['datetime'])[0][8:10]
                hour   = np.array(inpsection_dates[inpsection_dates['id'] == inspection]['datetime'])[0][11:13]
                minute = np.array(inpsection_dates[inpsection_dates['id'] == inspection]['datetime'])[0][14:16]
                second = np.array(inpsection_dates[inpsection_dates['id'] == inspection]['datetime'])[0][17:19]
                
                data.loc[rowIndex,'inspection_date'] = datetime(int(year), int(month), int(day), int(hour), int(minute), int(second))
                
            
            # Update variable names:
            data = data.rename(columns=dict([(value, key) for key, value in varnames(city)['inspection_properties'].items()]))
            
            self.data = data
            self.city = city
            
            # Add the depth and slope
            self.start_floorlevel = np.array(x2.start_floorlevel)[0]
            self.end_floorlevel   = np.array(x2.end_floorlevel)[0]

        else:
            print(data[varnames(city)['pipe_properties']['id']].unique()[0])
            raise ValueError('The information of the pipe is not consistent.')
            
    
    def getInfo(self):
        '''
        Get in a dictionary the properties
        '''
        a = deepcopy(vars(self))
        del a['data']
        return a
        
        
    def age(self):
        return datetime.datetime.now().year - self.construction_year
    
    
    def excludeClassCode(self,d,a,b):
        d2 = pd.DataFrame()
        for i in list(d.inspection_date.unique()):
            subd = d[d['inspection_date'] == i]
            subd2 = subd[np.in1d(list(subd[a]), b)]
            if subd2.empty:
                '''
                Here it was found that for certain inspection, there were 
                no codes or classes of interest. Therefore we have to add 
                the damage class "None" and code -1
                '''
                subd = subd.assign(damage_code="None")
                subd = subd.assign(damage_class=-1)
                subd2 = subd
            
            d2 = pd.concat([d2,subd2],axis=0)
        d = d2   
        return d
    
    def CleanDataBasedonCodes(self,imp_codes = [],imp_classes = []):
        
        d = deepcopy(self.data)
        
        if imp_codes != []: 
            # Exclude damage codes:
            d = self.excludeClassCode(d,'damage_code',imp_codes)
        if imp_classes != []: 
            # Exclude damage codes:
            d = self.excludeClassCode(d,'damage_class',imp_classes)
        
        return d
    
    def getClassCode_length(self,imp_codes = [],imp_classes = [], var = ['inspection_id','inspection_date','damage_class','damage_code','inspection_year']):
        '''
        Return a table in the rows the relative position where inspection was 
        carried out in the pipe, and in the columns are the:
            - Inspection year
            - Code
        In the data are the counts
        
        Variables:
        :param imp_codes:
        :param var:
        '''
        
        d = self.CleanDataBasedonCodes(imp_codes = imp_codes, imp_classes = imp_classes)
            
        a = []
        for i in var:
            a.append(d[i])
        t = pd.crosstab(d['position'],a)
        return t
    
    def AddicitonalFunction1(self,temp2,varss,c,cl,age):
        
        
        # Add the construction year to the data frame:
        temp2.loc[-1] = [pd.Timestamp(datetime(int(self.construction_year),1,1,0,0,0)), min(cl),0] # Here class 1 is the pristine condition.
        temp2.index = temp2.index + 1 
        temp2.sort_index(inplace=True) 
        temp1 = pd.pivot_table(temp2,columns=[varss['var1'],varss['var3']],values = [varss['var2']],aggfunc=lambda x:list(set(list(x))))
        # Change the name of the row names:
        temp1.columns.name = 'time'
        temp1 = temp1.set_axis(['state'], axis='index')
        
        '''
        # We check wheter any of the inspections has a class -1. If yes, this 
        means that during that inspection it was not found any code or class of 
        interest. The way we address this is by copying the damage classes of 
        the **previous** state.
        '''
        idx = [-1 in [j for i in [j] for j in i] for j in temp1.values[0]]
        if any(idx):
            for i in [i for i, x in enumerate(idx) if x == True]:
                temp1[temp1.columns[i]].state = temp1[temp1.columns[i-1]].state

        idx = list(temp1.values[0]) # This is the sequence of states conserving the temportal component.
        for i in range(0,len(idx)-1):
            c.loc[idx[i],idx[i+1]] = c.loc[idx[i],idx[i+1]] + 1 
    
        # Now we create the table that has the information of time and the transitions:
        t = list(temp1.columns.levels[temp1.T.index.names.index('inspection_date')])#temp1.columns.to_list()
        D = []
        for i in range(0,len(t)-1):
            D.append(t[i+1] - t[i])
        
        # Identify all the transitions:
        transitions = []
        for i in range(0,len(t)-1):
            f    = np.array(temp1[t[i+1]].T['state'])[0]   # Future state(s)
            p    = np.array(temp1[t[i]].T['state'])[0]     # Current state(s)
            comb = [(p[i1], f[j1]) for i1 in range(len(p)) for j1 in range(len(f))] # Make the permutation for all the states:
            transitions = transitions + comb
            
        transitions = sorted(list(set(transitions)))
        t1 = np.zeros((len(D),len(transitions))) # Create the table that stores the transitions
    
        # Create the transition matrix:
        for i in range(0,len(t)-1):
            f    = np.array(temp1[t[i+1]].T['state'])[0]   # Future state(s)
            p    = np.array(temp1[t[i]].T['state'])[0]     # Current state(s)
            comb = [(p[i1], f[j1]) for i1 in range(len(p)) for j1 in range(len(f))] # Make the permutation for all the states:
            for j in comb:
                k = transitions.index(j)
                t1[i,k] =int(1)
        
        t1 = pd.DataFrame(t1,columns = transitions)               
        t1.insert (0,age , list(itertools.accumulate(D)))
        
        idx1 = temp1.T.index.names.index('inspection_id')
        a = []
        for i in temp1.T.index.to_list():
            a.append(i[idx1])
            
        t1.insert (0,'inspection_id' , a[1:] )
        
        return temp1, t1
    
    
    #TODO: Ask Matthias how to put this function Class as an attribute of function getTransitionCounts
    # To be used as getTransitionCounts.Class()
    def getTransitionCounts_Class(self, cl=[], imp_codes = [], age = [],flag = []):
        '''
        Get the frequency tables per pipe object
        :param cl:
        :param imp_codes:
        '''
        
        varss = {'var1':'inspection_date','var2':'damage_class','var3':'inspection_id','var4':'damage_code'}      
        
        t = self.getClassCode_length(imp_codes = imp_codes,imp_classes = cl, var = list(varss.values()))
                
        if flag == 'most_critical':
            # Keep only the column with the maximum class
            aux4 = t.T.index.names.index(varss['var1'])
            aux5 = t.T.index.names.index(varss['var2'])
            
            aux3 = []
            for aux0 in np.unique([i[aux4] for i in t.columns.to_list()]):
                aux2 = []
                for i in t.columns.to_list():
                    if i[aux4] == aux0:
                        aux2.append(i)
                aux1 = [i[aux5] for i in aux2]
                idx  = aux1.index(max(aux1))
                aux3.append(aux2[idx])
                
            t = t.loc[:, t.columns.intersection(aux3)]
            

        idx={}
        col_names = np.array(t.columns.names)
        for i in range(0,len(varss.values())):
            exec("idx['var"+str(i+1)+"'] = list(col_names==varss['var"+str(i+1)+"']).index(True)")

        # Get the list of classes:
        if cl == []:
            cl = list(np.unique(t.columns.levels[list(np.array(t.columns.names)=='damage_class').index(True)]))
            if not np.any(np.array(cl)==0):
                # Add the state zero:
                cl.append(0)
                cl.sort()

        c = pd.DataFrame(np.zeros((len(cl), len(cl))),index = cl,columns=cl)
        temp1 = np.asarray(tuple(t.columns))
        
        temp2 = {}
        for i in varss.keys():
            exec("temp2['"+varss[i]+"'] = temp1[:,idx['"+i+"']]")
        temp2 = pd.DataFrame(temp2)
        temp3 = deepcopy(temp2)
        temp3.pop(varss['var4'])
        
        if self.construction_year != 'Unknown':
            temp1, t1 = self.AddicitonalFunction1(temp3,varss,c,cl,age)
        else:
            print('The construction year of the pipe is unknown')
            c = [pd.DataFrame(np.zeros((len(cl), len(cl))),index = cl,columns=cl)]
            temp1 = []
            t1 = []
        
        # Adjust the index of temp2:
        a1 = np.array([], dtype=int)
        cont = int(0)
        for i in  list(temp2.inspection_id.unique()):
            a1 = np.concatenate((a1, np.ones(sum(temp2.inspection_id == i),dtype=int)*cont))
            cont += 1
        temp2 = temp2.set_index(a1)
        
        #if np.any(t>1):
        #    #TODO: It happened that for the same position in the pipe the are 
        #    #multiple observations. We will take only the most critical one (i.e., highest class)
        #    raise ValueError('There are multiple observations with the same characteristics. Check function getTransitionCounts')
        
        return c , temp1, t1, temp2
        
    
    def coor(self,r=[]):
        if r is not []:
            r[r['node_id']==self.start_node_id]
            r[r['node_id']==self.end_node_id]
            z  = pd.DataFrame({'position':['start_node_id','end_node_id'],
                  'node_id':[r[r['node_id']==self.start_node_id]['node_id'].values[0],r[r['node_id']==self.end_node_id]['node_id'].values[0]],
                  'coor_x':[r[r['node_id']==self.start_node_id]['coor_x'].values[0],r[r['node_id']==self.end_node_id]['coor_x'].values[0]],
                  'coor_y':[r[r['node_id']==self.start_node_id]['coor_y'].values[0],r[r['node_id']==self.end_node_id]['coor_y'].values[0]],
                  'coor_z':[r[r['node_id']==self.start_node_id]['coor_z'].values[0],r[r['node_id']==self.end_node_id]['coor_z'].values[0]],
                  })
        else:
            raise ValueError('Provide the table of nodes in the variable all_nodes')
        return z
    
    # def getClassCodeLengths(self,imp_codes=[],imp_classes=[]):
        
    #     T = pd.DataFrame()
        
    #     # Leave only the data you want to work with (based on their class and codes)
    #     d = self.CleanDataBasedonCodes(imp_codes = imp_codes, imp_classes = imp_classes)
        
    #     d
        
    #     return T
    
                

        
    
