"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
import numpy as np
import random
#%%
def constraints_dtmc(typology=[]):

    if typology == 'A' or typology == 'I' or typology == 'Multi':

        def constraint1(x):
            sum_sq = 1
            for i in range(0,5):
                sum_sq = sum_sq - x[i]
            return sum_sq
        def constraint2(x):
            sum_sq = 1
            for i in range(5,9):
                sum_sq = sum_sq - x[i]
            return sum_sq
        def constraint3(x):
            sum_sq = 1
            for i in range(9,12):
                sum_sq = sum_sq - x[i]
            return sum_sq
        def constraint4(x):
            sum_sq = 1
            for i in range(12,14):
                sum_sq = sum_sq - x[i]
            return sum_sq
        
        # Constraints associated to the probability initial state vector:
        def constraint5(x):
            sum_sq = 1
            for i in range(14,19):
                sum_sq = sum_sq - x[i]
            return sum_sq
        
        # Constrains: p11 > p12 > p13 > p14 > p15 
        def constraint6(x):
            return x[0]-x[1]
        def constraint7(x):
            return x[1]-x[2]
        def constraint8(x):
            return x[2]-x[3]
        def constraint9(x):
            return x[3]-x[4]
        
        def constraint10(x):
            return x[5]-x[6]
        def constraint11(x):
            return x[6]-x[7]        
        def constraint12(x):
            return x[7]-x[8]   
    
        def constraint13(x):
            return x[9]-x[10]
        def constraint14(x):
            return x[10]-x[11]
        
        def constraint15(x):
            return x[12]-x[13]

        def constraint16(x):
            return 1-x[0]
        def constraint17(x):
            return 1-x[5]
        def constraint18(x):
            return 1-x[9]
        def constraint19(x):
            return 1-x[12]
  
        # Contraints:
        con1 = {'type': 'eq', 'fun': constraint1} # Contraints associated to the transitions from state 1
        con2 = {'type': 'eq', 'fun': constraint2} # Contraints associated to the transitions from state 2
        con3 = {'type': 'eq', 'fun': constraint3} # Contraints associated to the transitions from state 3
        con4 = {'type': 'eq', 'fun': constraint4} # Contraints associated to the transitions from state 4
        con5 = {'type': 'eq', 'fun': constraint5} # Contraints associated to the initial probability vector
        
        con6 = {'type': 'ineq', 'fun': constraint6}
        con7 = {'type': 'ineq', 'fun': constraint7}
        con8 = {'type': 'ineq', 'fun': constraint8}
        con9 = {'type': 'ineq', 'fun': constraint9}
        
        con10 = {'type': 'ineq', 'fun': constraint10}
        con11 = {'type': 'ineq', 'fun': constraint11}
        con12 = {'type': 'ineq', 'fun': constraint12}
        con13 = {'type': 'ineq', 'fun': constraint13}
        con14 = {'type': 'ineq', 'fun': constraint14}
        con15 = {'type': 'ineq', 'fun': constraint15}
    
        con16 = {'type': 'ineq', 'fun': constraint16}
        con17 = {'type': 'ineq', 'fun': constraint17}
        con18 = {'type': 'ineq', 'fun': constraint18}
        con19 = {'type': 'ineq', 'fun': constraint19}
    
    
        cons = [con1,con2,con3,con4,con5,
                con6,con7,con8,con9,
                con10,con11,con12,con13,con14,con15,
                con16,con17,con18,con19]

    elif typology == 'B' or typology == 'II' or typology == 'Single':
        
        # Constrains associated to the transitional probability matrix:
        def constraint1(x):
            return x[0]-0.5
        def constraint2(x):
            return x[1]-0.5
        def constraint3(x):
            return x[2]-0.5
        def constraint4(x):
            return x[3]-0.5

        con1 = {'type': 'ineq', 'fun': constraint1}
        con2 = {'type': 'ineq', 'fun': constraint2}
        con3 = {'type': 'ineq', 'fun': constraint3}
        con4 = {'type': 'ineq', 'fun': constraint4}
        
        # Constraints associated to the probability initial state vector:
        def constraint5(x):
            sum_sq = 1
            for i in range(0,5):
                sum_sq = sum_sq - x[len(x)-i-1]
            return sum_sq
        
        con5 = {'type': 'eq', 'fun': constraint5}
        
        
        cons = [con5]#[con1,con2,con3,con4,con5]
        
    return cons

def initial_parameters_dtmc(typology=[]):
    pi = []
    x0 = []
    if typology == 'A' or typology == 'I' or typology ==  'Multi':
        # Initial probability vector:
        #pi     = np.random.dirichlet(np.ones(5),size=1)[0]
        pi     = np.array([1,0,0,0,0])
        # Markov chain pararmeters:
        #x0     = np.concatenate((np.random.dirichlet(np.ones(5),size=1)[0],
        #                        np.random.dirichlet(np.ones(4),size=1)[0],
        #                       np.random.dirichlet(np.ones(3),size=1)[0],
        #                       np.random.dirichlet(np.ones(2),size=1)[0]),axis=0)
        x0     = np.array([1.,0,0,0,0,1.0,0,0,0,1.,0,0,1.,0]) 
        
    elif typology == 'B' or typology == 'II' or typology == 'Single':
        # Initial probability vector:
        #pi     = np.random.dirichlet(np.ones(5),size=1)[0]
        pi     = np.array([1,0,0,0,0])
        # Markov chain pararmeters:
        #x0     = np.random.dirichlet(np.ones(4),size=1)[0]  
        x0     = np.ones(4)
    return pi, x0


def getQ_topology_A(x):
    # Transitions from state 1:
    p11      = x[0]
    p12      = x[1]
    p13      = x[2]
    p14      = x[3]
    p15      = x[4]
    
    # Transitions from state 2:
    p22      = x[5]
    p23      = x[6]
    p24      = x[7]
    p25      = x[8]    

    # Transitions from state 3:
    p33      = x[9]
    p34      = x[10]
    p35      = x[11]

    # Transitions from state 4:
    p44      = x[12]
    p45      = x[13]
    
    # Transitions from state 5:
    p55      = 1

    # Transition probability matrix:
    Q        = np.array([[p11,p12,p13,p14,p15],
                         [0  ,p22,p23,p24,p25],
                         [0  ,0  ,p33,p34,p35],
                         [0  ,0  ,0  ,p44,p45],
                         [0  ,0  ,0  ,0  ,p55]]) # Transition probability matrix
    return Q
    
def getQ_topology_B(x):
    # Transitions from state 1:
    p11      = x[0]
    p22      = x[1]
    p33      = x[2]
    p44      = x[3]
    # Transition probability matrix
    Q        = np.array([[p11,1-p11,0      , 0     ,0    ],
                         [0  ,p22  , 1-p22 ,0      ,0    ],
                         [0  ,0    ,p33    , 1-p33 ,0    ],
                         [0  ,0    ,0      , p44   ,1-p44],
                         [0  ,0    ,0      , 0     ,1    ]]) 
    return Q



