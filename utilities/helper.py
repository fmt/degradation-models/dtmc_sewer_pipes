"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
import pandas as pd
import numpy as np
from datetime import timedelta
#%%
def flatten_list(_2d_list):
    flat_list = []
    # Iterate through the outer list
    for element in _2d_list:
        if type(element) is list:
            # If the element is of type list, iterate through the sublist
            for item in element:
                flat_list.append(item)
        else:
            flat_list.append(element)
    return flat_list
#%%
def method_CI_1(S_all,alpha=0.95):
    # Confidence intervals
    CI = {}
    for i in range(S_all.shape[2]):
        lower = np.percentile(S_all[:,:,i], ((1.0-alpha)/2.0) * 100,axis=0)
        lower[lower<0.0] = 0.0
        upper = np.percentile(S_all[:,:,i], (alpha+((1.0-alpha)/2.0)) * 100 ,axis=0)
        upper[upper>1.0] = 1.0
        CI['K_'+str(i+1)] = {'lower':lower,
                             'upper':upper} 
    return CI
#%% 
def table_probabilities_states_over_time(P,t = 1,damage_codes = [],damage_class=[],flag = []):
    
    P = P.set_index(np.arange(0, P.shape[0], 1, dtype=int))
    
    # Remove the data that is not associated to the damage code:
    if damage_codes != []: 
        # Exclude damage codes:
        P  = P[['pipe_id','inspection_id','pipe_age']+ damage_codes]

    # Counts vs Pipe age during inspection:  
    years = np.array(P['pipe_age'] / np.timedelta64(1, 'Y'))
    a,b = np.histogram(years, bins=np.arange(np.floor(min(years)), np.ceil(max(years)), t))
    # Counts vs Pipe age during inspection:
    
    CodeClass = list(P.columns[3:])        

    # Add columns of zeros for the missing classes in certain damage codes:
    c = [i[0]for i in CodeClass]
    for i in damage_codes:
        res = flatten_list([([idx for idx, val in enumerate(c) if val == sub] if sub in c else [None]) for sub in [i]])
        add_class = list(set([CodeClass[i][1]for i in res]) ^ set(damage_class))
        if len(add_class)>0:
            for j in add_class:
                col_zero = pd.DataFrame(list(np.zeros((P.loc[P.index].shape[0]),dtype=int)),
                                        columns=pd.MultiIndex.from_product([[i],
                                                                        [j]]))
                P = pd.concat([P,col_zero], axis=1)
    

    # Make a matrix only with the damage classes:
    CodeClass = list(P.columns[3:])
    c = [i[1]for i in CodeClass]
    R = pd.DataFrame()
    for i in damage_class:
        res = flatten_list([([idx for idx, val in enumerate(c) if val == sub] if sub in c else [None]) for sub in [i]])
        R[i] = (P[[CodeClass[i] for i in res]].sum(axis=1)>0)*1
    
    P = pd.concat([  P[['pipe_id','inspection_id','pipe_age']].droplevel(1, axis=1) , R], axis=1)
    
    
    # Add 1 to class 1 if no damage was found during the inspection:
    idx = P.index[P[damage_class].sum(axis=1)==0]
    P.loc[(idx),1] =  list(np.ones((idx.shape[0]),dtype=int))
    
    if flag == 'most_critical':
        # We consider only the most critical state found during the inspection:
        df = P[damage_class]
        #df.loc[df.index,(damage_class)] = df.loc[df.index,(damage_class)]*damage_class
        df = df.loc[df.index,(damage_class)]*damage_class
        affected = (df != 0).any(axis=1)
        nz = df[affected]
        #df.loc[(df.index[affected == True]),(damage_class)] = (nz.T == nz.max(axis=1)).T.astype(int)
        df = (nz.T == nz.max(axis=1)).T.astype(int)
        # Update the database with the new condition:
        P  = pd.concat([  P[['pipe_id','inspection_id','pipe_age']], df], axis=1)
    

    pt = pd.DataFrame()
    
    # Add a row of zeros to start in the Year 0
    P.loc[0] = 0
    P.loc[0, ('pipe_age')] = timedelta(days=0)

    # Age mean:
    pt.insert(0, "Age mean", ((P.groupby(pd.Grouper(key='pipe_age',freq=str(int(t*365))+'D'))['pipe_age'].max()+
                              P.groupby(pd.Grouper(key='pipe_age',freq=str(int(t*365))+'D'))['pipe_age'].min())/2)/ np.timedelta64(1,'Y') )    
    # Add the intervals:
    pt.insert(0, "Age_f", np.array(P.groupby(pd.Grouper(key='pipe_age',freq=str(int(t*365))+'D'))['pipe_age'].max()/np.timedelta64(1,'Y')))
    pt.insert(0, "Age_i", np.array(P.groupby(pd.Grouper(key='pipe_age',freq=str(int(t*365))+'D'))['pipe_age'].min()/np.timedelta64(1,'Y')))
    # Counts:
    pt.insert(0, "count", P.groupby(pd.Grouper(key='pipe_age',freq=str(int(t*365))+'D'))[damage_class].sum().sum(axis=1))

    # Frequencies:
    pt_frequencies = P.groupby(pd.Grouper(key='pipe_age',freq=str(int(t*365))+'D'))[damage_class].mean()

    # Concatenate and delete empty rows:
    Pt_mean      = pd.concat([pt,pt_frequencies],axis=1)
        
    # Reset the index:
    Pt_mean      = Pt_mean.set_index(np.arange(0, Pt_mean.shape[0], 1, dtype=int)).dropna()


    return Pt_mean