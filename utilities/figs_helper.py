"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
import pickle
import numpy as np
from utilities.dtmc import prediction, expectation
import pandas as pd
from utilities.helper import table_probabilities_states_over_time as discretize
import matplotlib.pyplot as plt
import os 
from os.path import exists
from decimal import Decimal
from tqdm import tqdm
#%%
def load_cases(setup):
    if setup == 'comparing_cohorts':
        c = []
        for chains in ['Multi','Single']:
            for cases in [['CMW','CR'],['PMW','PR'],['CdL','CdG']]:
                for damage_code in ['BBF','BAF','BAB']:
                    c.append(['cohort_' + cases[0] + '_chain_' + chains+'_damagecode_'+damage_code,
                         'cohort_' + cases[1] + '_chain_' + chains+'_damagecode_'+damage_code])
                    
    elif setup == 'comparing_chains':
        c = []
        for cases in ['CMW','CR','PMW','PR','CdL','CdG']:
            for chains in [['Multi','Single']]:
                for damage_code in ['BBF','BAF','BAB']:
                    c.append(['cohort_' + cases + '_chain_' + chains[0] +'_damagecode_'+damage_code,
                         'cohort_' + cases + '_chain_' + chains[1] +'_damagecode_'+damage_code])
    return c

def customize_plots():
    
    g = {
        'font_name':'Dejavu Sans',
        'font_size_axes': 22,
        'font_size_label': 16,
        }
    
    c = {}
    
    # Colors when comparing cohorts:
    c['comparing_cohorts'] = ['mediumblue',
                              'crimson']
    
    # Colors when comparing expectations:
    c['comparing_expectations'] = ['lightseagreen',
                              'deeppink',
                              'darkolivegreen',
                              'orange',
                              'magenta',
                              ]

    # Colors when comparing expectations:
    c['comparing_chains'] = ['darkgreen',
                              'darkviolet',
                              ]
    
    return g,c
#%%

def load_all_data(list_cases,dir_DTMCs_location,t,s,analysis,ins_data):
    data = {}
    for i in list_cases:
            # Check if the file exists in the folder:
            if exists(dir_DTMCs_location + i):
                # Get the projections and discretize table:
            
                S_all_projection, S_all_expectation, S_all_P, S_all_S0 = aux_func_DTMCs_proj_exp_P(i,dir_DTMCs_location,t,step=1)
                    
                data[i] = {
                    'dtmcs': S_all_projection,#aux_func_DTMCs_projections(i,dir_DTMCs_location,t,step=s),
                    'expectation': S_all_expectation,#aux_func_DTMCs_expectations(i,dir_DTMCs_location,t,step=s),
                    'P':S_all_P,
                    'S0':S_all_S0,
                    't':t,
                    'cohort_name':analysis[i]['cohort_name'],
                    'Sk': aux_func_ground_truth_discretized(ins_data,i,analysis,step=s),
                    'cohort_chain':analysis[i]['cohort_chain'],
                    }
            else:
                print('The file for the case: ' + i + ' does not exist.')
    return data



def load_inspection_data_set(path,files):
    data = {}
    for i in files:
        data[i] = pickle.load(open(path+i,"rb"))
    return data
    

def method_CI_1(S_all,plot_type,alpha=0.95):
    # Confidence intervals
    CI = {}
    if plot_type == 'dtmcs':
        for i in range(S_all.shape[2]):
            lower = np.percentile(S_all[:,:,i], ((1.0-alpha)/2.0) * 100,axis=0)
            lower[lower<0.0] = 0.0
            upper = np.percentile(S_all[:,:,i], (alpha+((1.0-alpha)/2.0)) * 100 ,axis=0)
            upper[upper>1.0] = 1.0
            CI['K_'+str(i+1)] = {'lower':lower,
                                 'upper':upper} 
    elif plot_type == 'expectation':
            lower = np.percentile(S_all, ((1.0-alpha)/2.0) * 100,axis=0)
            #lower[lower<0.0] = 0.0
            upper = np.percentile(S_all, (alpha+((1.0-alpha)/2.0)) * 100 ,axis=0)
            #upper[upper>1.0] = 1.0
            CI['K'] = {'lower':lower,
                                 'upper':upper} 
    return CI



def aux_func_DTMCs_proj_exp_P(c,path,t,step=1):
    # Load the DTMCs
    DTMCs = pickle.load(open(path+c,"rb"))
    
    S_all_projection  = []
    S_all_expectation = []
    S_all_P           = []
    S_all_S0           = []

    for n in  tqdm(range(len(DTMCs))):
        s  = []
        e  = []
        p  = []
        s0 = []
        for j in list(DTMCs[n]['mc_models'].keys()):
            P  = DTMCs[n]['mc_models'][j]['P']
            S0 = DTMCs[n]['mc_models'][j]['p0']
            S  = prediction(P,S0,t,step/DTMCs[n]['Delta_t'])
            E  = expectation(t,P,S0,step/DTMCs[n]['Delta_t'])
            s.append(S)
            e.append(E)
            p.append(P)
            s0.append(S0)
        S_all_projection.append(np.array(s))
        S_all_expectation.append(np.array(e))
        S_all_P.append(np.array(p))
        S_all_S0.append(np.array(s0))
        
    # Stack all the projections in S_all into a single tensor.
    S_all_projection  = np.vstack(S_all_projection)
    S_all_expectation = np.vstack(S_all_expectation)
    S_all_P = np.vstack(S_all_P)
    S_all_S0 = np.vstack(S_all_S0)
    
    return S_all_projection, S_all_expectation, S_all_P, S_all_S0


def aux_func_ground_truth_discretized(ins_data,c,a,step=1):
    
    covariates  = ins_data['covariates']
    inspection  = ins_data['inspection']
    d = inspection[inspection['pipe_id'].isin(covariates['pipe_id'][a[c]['idx']])]
    pt = discretize(d,
                    t=step,
                    damage_codes=a[c]['codes'],
                    damage_class=[1,2,3,4,5],
                    flag = 'most_critical')
    return pt
    
    
    
    
#%% Plots functions
    
def plot_DTMCs(dtmcs,key,colors,key2=[],k=1):
    # Compute confidence intervals:
    CI = method_CI_1(dtmcs[key],plot_type=key)
    
    if key == 'dtmcs':
        plt.plot(dtmcs['t'],dtmcs[key][:,:,k-1].T,alpha=0.0075,color=colors,zorder = 1)
        # Plot the confidence intervals (projections):
        if key2 == []:
            plt.plot(dtmcs['t'],np.median(dtmcs[key][:,:,k-1],axis=0), '-', color=colors, alpha=1,zorder = 3,label =dtmcs['cohort_name'].replace('_',' '))
        else:
            plt.plot(dtmcs['t'],np.median(dtmcs[key][:,:,k-1],axis=0), '-', color=colors, alpha=1,zorder = 3,label = 'Chain ' + dtmcs['cohort_chain'].replace('_',' '))
        plt.plot(dtmcs['t'], CI['K_'+str(k)]['lower'], '--', color=colors, alpha=1,zorder = 3)
        plt.plot(dtmcs['t'], CI['K_'+str(k)]['upper'], '--', color=colors, alpha=1,zorder = 3)
    elif key == 'expectation':
        plt.plot(dtmcs['t'],dtmcs[key].T,alpha=0.0075,color=colors,zorder = 1)
        # Plot the confidence intervals (projections):
        plt.plot(dtmcs['t'],np.median(dtmcs[key],axis=0), '-', color=colors, alpha=1,zorder = 3,label =dtmcs['cohort_name'].replace('_',' '))
        plt.plot(dtmcs['t'], CI['K']['lower'], '--', color=colors, alpha=1,zorder = 3)
        plt.plot(dtmcs['t'], CI['K']['upper'], '--', color=colors, alpha=1,zorder = 3)
        

def plot_discretize_table(dtmcs,colors,k=1):
    # Compute confidence intervals:
    a = 0.35
    scale_f = 5
    plt.scatter(np.array(dtmcs['Sk']['Age mean']),
                np.array(dtmcs['Sk'][k]),
                s=np.array(dtmcs['Sk']['count'])/scale_f,
                marker = 'o',
                color= colors,
                alpha=a,
                zorder = 4)
    
def comparing_cohorts_plots(i,analysis,data,k=1,path_save=[]):
       
    # Load common features:
    general_plot,colors= customize_plots()

    # Plot comparison between cohorts:
    plt.figure(figsize=(4, 4))
    plt.rcParams.update({'font.size': general_plot['font_size_axes'],
                         'font.family': general_plot['font_name']})
    cont = -1
    damages = []
    chains  = []
    cohorts = []

    for j in i:
        
        cohorts.append(analysis[j]['cohort_name'])
        damages.append(analysis[j]['codes'])
        chains.append(analysis[j]['topology'])
        
        cont += 1
        # Plot projections
        plot_DTMCs(data[j],key='dtmcs',colors = colors['comparing_cohorts'][cont],key2=[],k=k)
        # Add ground truth:
        plot_discretize_table(data[j],colors = colors['comparing_cohorts'][cont],k=k)
    
    plt.ylim([0,1])
    plt.xlim([min(data[j]['t']),max(data[j]['t'])])
    plt.xticks(np.arange(min(data[j]['t']),max(data[j]['t'])+1,25))
    plt.ylabel('$S^{(t)}_'+str(k)+'$',{'fontname':general_plot['font_name']})
    plt.xlabel('PipeAge [Years]')
    
    damages = list(set([item for sublist in damages for item in sublist]))
    chains  = list(set(chains))
    cohorts  = list(set(cohorts))
    plt.title('+'.join(damages) + ', '+ 'Chain '+ '+'.join(chains) + ', $k$ = ' + str(k),
              {'fontname':general_plot['font_name'],
               'fontsize':general_plot['font_size_axes']})
    
    # Legend: Lines
    leg1 = plt.legend(ncol=2,
                      loc="upper center",
                      framealpha=1,
                      fontsize=general_plot['font_size_label'],
                      shadow=True,
                      fancybox=True,
                      bbox_to_anchor=(-0.3,1.05,1.3,0.2),mode="expand", borderaxespad=0)
    
    # Legend: Markers
    a = 0.35
    scale_f = 5
    size1 = plt.scatter([],[], s=50/scale_f, marker='o', color='black',alpha=a)
    size2 = plt.scatter([],[], s=500/scale_f, marker='o', color='black',alpha=a)
    size3 = plt.scatter([],[], s=2000/scale_f, marker='o', color='black',alpha=a)
   
    plt.legend((size1,size2,size3),
               ('50', '500', '2000'),
               scatterpoints=1,
               framealpha=0.5,
               loc='best',
               fancybox=True,
               ncol=1,
               fontsize=16)

    plt.gca().add_artist(leg1)
    
    if path_save != []:
        # Check if a folder with the name of the cohort exist
        if not os.path.exists(path_save):
            os.makedirs(path_save)
        plt.savefig(path_save + '/comparison_'+ '_'.join(cohorts) +'_damagecode_'+'_'.join(damages)+'_chain_'+'_'.join(chains)+'_k_'+ str(k) +'.pdf', bbox_inches="tight")
        plt.savefig(path_save + '/comparison_'+ '_'.join(cohorts) +'_damagecode_'+'_'.join(damages)+'_chain_'+'_'.join(chains)+'_k_'+ str(k) +'.png', bbox_inches="tight")
        plt.savefig(path_save + '/comparison_'+ '_'.join(cohorts) +'_damagecode_'+'_'.join(damages)+'_chain_'+'_'.join(chains)+'_k_'+ str(k) +'.jpeg', bbox_inches="tight")
        plt.close()
        

def comparing_expectations_DTMCs_plots(i,analysis,data,k=1,path_save=[]):

    # Load common features:
    general_plot,colors= customize_plots()

    # Plot comparison between cohorts:
    plt.figure(figsize=(4, 4))
    plt.rcParams.update({'font.size': general_plot['font_size_axes'],
                         'font.family': general_plot['font_name']})
    cont = -1
    damages = []
    chains  = []
    cohorts = []

    for j in i:
        
        cohorts.append(analysis[j]['cohort_name'])
        damages.append(analysis[j]['codes'])
        chains.append(analysis[j]['topology'])
        
        cont += 1
        # Plot projections
        plot_DTMCs(data[j],key='expectation',colors = colors['comparing_expectations'][cont],key2=[],k=k)
    
    plt.xlim([min(data[j]['t']),max(data[j]['t'])])
    plt.ylim([1,5])
    plt.xticks(np.arange(min(data[j]['t']),max(data[j]['t'])+1,25))
    plt.yticks(np.arange(1,6,1))
    plt.ylabel('Expected Severity Class',{'fontname':general_plot['font_name']})
    plt.xlabel('PipeAge [Years]')
    
    damages = list(set([item for sublist in damages for item in sublist]))
    chains  = list(set(chains))
    cohorts  = list(set(cohorts))
    plt.title('+'.join(damages) + ', '+ 'Chain '+ '+'.join(chains),
              {'fontname':general_plot['font_name'],
               'fontsize':general_plot['font_size_axes']})
    
    # Legend: Lines
    leg1 = plt.legend(ncol=2,
                      loc="upper center",
                      framealpha=1,
                      fontsize=general_plot['font_size_label'],
                      shadow=True,
                      fancybox=True,
                      bbox_to_anchor=(-0.3,0.95,1.4,0.3),mode="expand", borderaxespad=0)
    
    plt.gca().add_artist(leg1)
    
    if path_save != []:
        # Check if a folder with the name of the cohort exist
        if not os.path.exists(path_save):
            os.makedirs(path_save)
        plt.savefig(path_save + '/comparison_expectations_'+ '_'.join(cohorts) +'_damage_code_'+'_'.join(damages)+'_chain_'+'+'.join(chains)+'.pdf', bbox_inches="tight")
        plt.savefig(path_save + '/comparison_expectations_'+ '_'.join(cohorts) +'_damage_code_'+'_'.join(damages)+'_chain_'+'+'.join(chains)+'.png', bbox_inches="tight")
        plt.savefig(path_save + '/comparison_expectations_'+ '_'.join(cohorts) +'_damage_code_'+'_'.join(damages)+'_chain_'+'+'.join(chains)+'.jpeg', bbox_inches="tight")
        plt.close()
    

def comparing_chains_DTMCs_plots(i,analysis,data,k=1,path_save=[]):
       
    # Load common features:
    general_plot,colors= customize_plots()

    # Plot comparison between cohorts:
    plt.figure(figsize=(4, 4))
    plt.rcParams.update({'font.size': general_plot['font_size_axes'],
                         'font.family': general_plot['font_name']})
    cont = -1
    damages = []
    chains  = []
    cohorts = []

    for j in i:
        
        cohorts.append(analysis[j]['cohort_name'])
        damages.append(analysis[j]['codes'])
        chains.append(analysis[j]['topology'])
        
        cont += 1
        # Plot projections
        plot_DTMCs(data[j],key='dtmcs',colors = colors['comparing_chains'][cont],key2='chain',k=k)
        # Add ground truth:
        plot_discretize_table(data[j],colors = colors['comparing_chains'][cont],k=k)
    
    plt.ylim([0,1])
    plt.xlim([min(data[j]['t']),max(data[j]['t'])])
    plt.xticks(np.arange(min(data[j]['t']),max(data[j]['t'])+1,25))
    plt.ylabel('$S^{(t)}_'+str(k)+'$',{'fontname':general_plot['font_name']})
    plt.xlabel('PipeAge [Years]')
    
    damages = list(set([item for sublist in damages for item in sublist]))
    chains  = list(set(chains))
    cohorts  = list(set(cohorts))
    plt.title('+'.join(damages) + ', '+ '+'.join(cohorts).replace('_',' ') + ', $k$ = ' + str(k),
              {'fontname':general_plot['font_name'],
               'fontsize':general_plot['font_size_axes']})
    
    # Legend: Lines
    leg1 = plt.legend(ncol=2,
                      loc="upper center",
                      framealpha=1,
                      fontsize=general_plot['font_size_label'],
                      shadow=True,
                      fancybox=True,
                      bbox_to_anchor=(-0.3,1.05,1.3,0.2),mode="expand", borderaxespad=0)
    
    # Legend: Markers
    a = 0.35
    scale_f = 5
    size1 = plt.scatter([],[], s=50/scale_f, marker='o', color='black',alpha=a)
    size2 = plt.scatter([],[], s=500/scale_f, marker='o', color='black',alpha=a)
    size3 = plt.scatter([],[], s=2000/scale_f, marker='o', color='black',alpha=a)
   
    plt.legend((size1,size2,size3),
               ('50', '500', '2000'),
               scatterpoints=1,
               framealpha=0.5,
               loc='best',
               fancybox=True,
               ncol=1,
               fontsize=16)

    plt.gca().add_artist(leg1)
    
    if path_save != []:
        # Check if a folder with the name of the cohort exist
        if not os.path.exists(path_save):
            os.makedirs(path_save)
        plt.savefig(path_save + '/comparison_cohort_'+ '_'.join(cohorts) +'_damage_code_'+'_'.join(damages)+'_k_'+ str(k) +'.pdf', bbox_inches="tight")
        plt.savefig(path_save + '/comparison_cohort_'+ '_'.join(cohorts) +'_damage_code_'+'_'.join(damages)+'_k_'+ str(k) +'.png', bbox_inches="tight")
        plt.savefig(path_save + '/comparison_cohort_'+ '_'.join(cohorts) +'_damage_code_'+'_'.join(damages)+'_k_'+ str(k) +'.jpeg', bbox_inches="tight")
        plt.close()
    
def comparing_trans_probabilities_DTMCs_plots(i,analysis,data,k=1,path_save=[]):
    
    # Load common features:
    general_plot,colors= customize_plots()

    # Plot comparison between cohorts:
    
    plt.rcParams.update({'font.size': general_plot['font_size_axes'],
                         'font.family': general_plot['font_name']})


    # Identify the number of states:
    s = []
    for j in data.keys():
        s.append(data[j]['P'].shape)
    s = np.array(s)
    
    # Statistics from the transition probability matrices:
    mean    = np.mean(data[j]['P'],axis=0)       
    std_dev    = np.std(data[j]['P'],axis=0)       
    
    # Plot the parameters of the transitio probability matrix.
    if np.unique(s,axis=0).shape[0] == 1:
        _,m1,n1 = np.unique(s,axis=0)[0]
        for m in range(m1):
            for n in range(n1):
                
                plt.figure(figsize=(4, 4))
                all_data = [] 
                cont = -1
                damages = []
                chains  = []
                cohorts = []
                for j in i:

                    cohorts.append(analysis[j]['cohort_name'])
                    damages.append(analysis[j]['codes'])
                    chains.append(analysis[j]['topology'])
                    cont += 1

                    if np.mean(data[j]['P'],axis=0)[m,n] != 0:
                        all_data.append(data[j]['P'][:,m,n])
                        plt.hist(data[j]['P'][:,m,n], bins = 50,alpha = 0.7,label = 'Chain ' + analysis[j]['topology'],zorder = 0,color=colors['comparing_chains'][cont])
                        plt.plot([],[], marker='_',color=colors['comparing_chains'][cont],label='$\mu$: ' + "{:.2E}".format(Decimal(str(np.mean(data[j]['P'],axis=0)[m,n]))) + ' $\sigma$: ' +"{:.2E}".format(Decimal(str(np.std(data[j]['P'],axis=0)[m,n]))) )  
                    
                # Save the plot:
                if all_data != []:
                    a = np.concatenate(all_data,axis=0)
                    plt.xlim([min(a),max(a)])
                    plt.xticks(np.linspace(min(a),max(a),5))
                    plt.xticks(rotation = 45)
                    plt.ylabel('Counts',{'fontname':general_plot['font_name']})
                    plt.xlabel('Probability')
                    
                    damages = list(set([item for sublist in damages for item in sublist]))
                    chains  = list(set(chains))
                    cohorts  = list(set(cohorts))
                    
                    plt.title('+'.join(damages) + ', '+ '+'.join(cohorts).replace('_',' ') + ', $P_{('+str(m+1)+'}$,$_{'+str(n+1)+')}$ ',
                              {'fontname':general_plot['font_name'],
                               'fontsize':general_plot['font_size_axes']})
                    
                    # Legend: Lines
                    leg1 = plt.legend(ncol=2,
                                      loc="upper center",
                                      framealpha=1,
                                      fontsize=general_plot['font_size_label'],
                                      shadow=True,
                                      fancybox=True,
                                      bbox_to_anchor=(-0.50,1.2,2.0,0.2),mode="expand", borderaxespad=0)
                    
                    if path_save != []:
                        # Check if a folder with the name of the cohort exist
                        if not os.path.exists(path_save):
                            os.makedirs(path_save)
                        plt.savefig(path_save + '/cohort_'+ '_'.join(cohorts) +'_damagecode_'+'_'.join(damages)+'_P_'+ str(m+1) +'_'+str(n+1)+'.pdf', bbox_inches="tight")
                        plt.savefig(path_save + '/cohort_'+ '_'.join(cohorts) +'_damagecode_'+'_'.join(damages)+'_P_'+ str(m+1) +'_'+str(n+1)+'.png', bbox_inches="tight")
                        plt.savefig(path_save + '/cohort_'+ '_'.join(cohorts) +'_damagecode_'+'_'.join(damages)+'_P_'+ str(m+1) +'_'+str(n+1)+'.jpeg', bbox_inches="tight")
                        plt.close()
    else:
        print('Error: the Markov Chains have different dimenssions... ')
        
    
    # Plot the initial state vector:
    if m1 == n1:
        for m in range(m1):
            
            if m == 0:
                m
            
            plt.figure(figsize=(4, 4))
            all_data = [] 
            cont = -1
            damages = []
            chains  = []
            cohorts = []
            for j in i:
                cohorts.append(analysis[j]['cohort_name'])
                damages.append(analysis[j]['codes'])
                chains.append(analysis[j]['topology'])
                cont += 1
                all_data.append(data[j]['S0'][:,m]) 
                plt.hist(data[j]['S0'][:,m], bins = 50,alpha = 0.7,label = 'Chain ' + analysis[j]['topology'],zorder = 0,color=colors['comparing_chains'][cont])
                plt.plot([],[], marker='_',color=colors['comparing_chains'][cont],label='$\mu$: ' + "{:.2E}".format(Decimal(str(np.mean(data[j]['S0'][:,m],axis=0)))) + ' $\sigma$: ' + "{:.2E}".format(Decimal(str(np.std(data[j]['S0'][:,m],axis=0)))) )  
            
            if all_data != []:
                a = np.concatenate(all_data,axis=0)
                plt.xlim([min(a),max(a)])
                plt.xticks(np.linspace(min(a),max(a),5))
                plt.xticks(rotation = 45)
                plt.ylabel('Counts',{'fontname':general_plot['font_name']})
                plt.xlabel('Probability')
                
                damages = list(set([item for sublist in damages for item in sublist]))
                chains  = list(set(chains))
                cohorts  = list(set(cohorts))
                
                plt.title('+'.join(damages) + ', '+ '+'.join(cohorts).replace('_',' ') + ', $S_{'+str(m+1)+'}$',
                          {'fontname':general_plot['font_name'],
                           'fontsize':general_plot['font_size_axes']})
                
                # Legend: Lines
                leg1 = plt.legend(ncol=2,
                                  loc="upper center",
                                  framealpha=1,
                                  fontsize=general_plot['font_size_label'],
                                  shadow=True,
                                  fancybox=True,
                                  bbox_to_anchor=(-0.5,1.2,2.0,0.2),mode="expand", borderaxespad=0)
                
                if path_save != []:
                    # Check if a folder with the name of the cohort exist
                    if not os.path.exists(path_save):
                        os.makedirs(path_save)
                    plt.savefig(path_save + '/cohort_'+ '_'.join(cohorts) +'_damage_code_'+'_'.join(damages)+'_S_'+ str(m+1) +'.pdf', bbox_inches="tight")
                    plt.close()
                
                

def master_plots_DTMCs(c,analysis,data,save_figures_path,states=[1],save_plot = 0,plot_type='comparing_cohorts'):
    for i in c:
        for k in states:
            # Comparing cohorts:
            cohorts = []
            for j in i:
                cohorts.append(analysis[j]['cohort_name'])
            cohorts  = list(set(cohorts))
                
            # Comparing damages:
            damages = []
            for j in i:
                damages.append(analysis[j]['codes'])
            damages = list(set([item for sublist in damages for item in sublist]))
                
            # Comparing chains:
            chains = []
            for j in i:
                chains.append(analysis[j]['topology'])
            chains  = list(set(chains))
                
            if save_plot == 0:
                path_save = []
            else:
                if plot_type == 'comparing_cohorts':
                    path_save = save_figures_path + 'comparing_cohorts' + '/' + 'comparing_' + '_'.join(cohorts) + '/damage_code_' + '_'.join(damages) + '/chain_' + '_'.join(chains)
                elif plot_type == 'comparing_expectations':
                    path_save = save_figures_path + 'comparing_expectations' + '/' + 'comparing_' + '_'.join(cohorts) + '/damage_code_' + '_'.join(damages) + '/chain_' + '_'.join(chains)
                elif plot_type == 'comparing_chains':
                    path_save = save_figures_path + 'comparing_chains' + '/' + 'comparing_' + '_'.join(cohorts) + '/damage_code_' + '_'.join(damages) + '/chain_' + '_'.join(chains)
                elif plot_type == 'comparing_transition_probabilities':
                    path_save = save_figures_path + 'comparing_trans_probabilities' + '/' + 'comparing_' + '_'.join(cohorts) + '/damage_code_' + '_'.join(damages) + '/chain_' + '_'.join(chains)
                    
                    
            # Create the plots:
            if plot_type == 'comparing_cohorts':
                comparing_cohorts_plots(i,analysis,data,k=k,path_save = path_save)
            elif plot_type == 'comparing_expectations':
                comparing_expectations_DTMCs_plots(i,analysis,data,k=k,path_save = path_save)
            elif plot_type == 'comparing_chains':
                comparing_chains_DTMCs_plots(i,analysis,data,k=k,path_save = path_save)
            elif plot_type == 'comparing_transition_probabilities':
                comparing_trans_probabilities_DTMCs_plots(i,analysis,data,k=k,path_save = path_save)
                
            
            
            

    
    

