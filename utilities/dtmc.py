"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
from utilities.constraints_dtmc import constraints_dtmc, initial_parameters_dtmc
import numpy as np
from scipy.optimize import minimize
from scipy.linalg import fractional_matrix_power as multMat
import time

#%%
def cost_function(p,pt,c):
    #error_ = mean_squared_error(np.multiply(p.T,k/max(k)).T, np.multiply(pt.T,k/max(k)).T, squared=False) 
    #Modified May 10, 2022
    #D = (np.multiply(((p - pt)).T,c/max(c).T)**2).flatten()
    #RWMSE = np.sqrt(sum(D)/len(D))
    
    
    # Number of time steps
    n   = p.shape[0] 
    # Number of states
    k   = p.shape[1] 
    # Weight vector:
    w   = (c/max(c)).reshape(n,1)
    # Square Error:
    SQE = (p - pt)**2    
    #SQE = (p - pt)
    # Mean Weighted Square Error:
    WMSE = sum((SQE * w).flatten()) / (n*k)
    #WMSE = sum(( (SQE * w)**2 ).flatten()) / (n*k)
    # Root Mean Weighted Square Error
    RWMSE = np.sqrt(WMSE)
    return RWMSE
#%%
def expectation(n=[],Qd=[],pi=[],z=1):
    return np.sum(np.multiply(prediction(Qd,pi,n,z),np.array([1,2,3,4,5])),axis=1)
#%%
def minimize_this_function(x,*args):
    
    pt,n,k,topology = args
    
    if topology == 'A' or topology == 'I' or topology == 'Multi':
        from utilities.constraints_dtmc import getQ_topology_A as getQ
    elif topology == 'B' or topology == 'II' or topology == 'Single':
        from utilities.constraints_dtmc import getQ_topology_B as getQ

    pi        = x[-5:]
    Q         = getQ(x[0:-5])
    p         = prediction(Q,pi,n)
    rmse      = cost_function(p,pt,k)
    return rmse
#%%
def prediction(P=[],S0=[],n=[],z=1):
    
    '''
    Prediction using DTMC
    
    Input:
        P  :  Transition Probability Matrix
        S0 :  Initial Probability Vector
        n  :  Step vector
        z  :  Relation between Dt's ( Dt_training / Dt_requested )
            
    Output:
        Sn :  Projected probabilities from the DTMC based on the Chapman-Kolmagorov equation
    '''
    
    Sn = []
    if z == 1:
        # This means that the Dt from the trained DTMC and the asked projection is the same.
        for n in n:
            if n >= 0:
                Sn.append(S0.dot(np.linalg.matrix_power(P,n)).T)
            else:
                a = np.empty(S0.shape)
                a[:] = np.NaN
                Sn.append(a)   
    else:
        # This means that Dt we need to scalate the n vector.
        for s in list(n*z):
            Sn.append(S0.dot(multMat(P,s)).T)       
    return np.array(Sn)    
#%%
def dtmc(dataset = [],topology = 'A',n_repetitions=10):    
           
    
    if topology == 'A' or topology == 'I' or topology == 'Multi':
        from utilities.constraints_dtmc import getQ_topology_A as getQ
    elif topology == 'B' or topology == 'II'  or topology == 'Single':
        from utilities.constraints_dtmc import getQ_topology_B as getQ

    pt = dataset['proportions_per_class']
    n  = dataset['discretized_n_step_vector']
    k  = dataset['counts']
    #%% Calibration of the discrete-time Markov chain:
    
    # Load the constraints:
    cons = constraints_dtmc(topology)  
    
    r = {}
    e = []
    times = []
    for i in range(0,n_repetitions):
        
        t = time.time()
        pi,x0 = initial_parameters_dtmc(topology)
        
        # Here we join the initial paramers and the Markov chain parameters:
        x0 = np.concatenate((x0,pi))
        
        b    = (0.0,1.0)
        bnds = []
        for j in range(0,len(x0)):
            bnds.append(b)
        
        # Sequential Least Squares Programming (SLSQP).
        sol  = minimize(minimize_this_function,x0,args=(pt,n,k,topology),method='SLSQP',
                        bounds=bnds,
                        constraints=cons)
        
        # Save the results form the optimization process
        r[i] = {'P':getQ(sol.x[0:-5]),'p0':sol.x[-5:],'error':sol.fun}
        e.append(sol.fun)
        times.append(time.time() - t) # Time to converge

    return r,e,times