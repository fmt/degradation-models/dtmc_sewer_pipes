"""
Created on June 20, 2022

Source paper: 
Deterioration modeling of sewer pipes via discrete-time Markov chains: 
A large-scale case study in the Netherlands

@author: Lisandro A. Jimenez-Roa 
e-mail: l.jimenezroa@utwente.nl
"""
#%% Load libraries:
def varnames(case = ''):
    
    '''
    The dictionary below makes a relation between the name of the variables used 
    directly in the database (right), and the name of the variables we want to use
    (i.e., dictionary keys, left).
    '''
    
    if case == 'breda':
        # Variable names:
        varnames = {}
        pipe_properties = {}
        inspection_properties = {}
        add_var_inspection_properties = {}
        #%% Pipe propertiers (variables name directly from the dataset):
        pipe_properties['construction_year']                            = 'pipe_construction_year' 
        pipe_properties['pipe_id']                                      = 'sewer_pipe_id'  # pipe_id
        pipe_properties['length']                                       = 'pipe_length'
        pipe_properties['shape']                                        = 'pipeshape'
        pipe_properties['width']                                        = 'width'
        pipe_properties['material']                                     = 'material'
        pipe_properties['height']                                       = 'height'
        pipe_properties['system_type']                                  = 'systemtype'
        pipe_properties['function']                                     = 'pipefunction'
        pipe_properties['content']                                      = 'contentstype'
        pipe_properties['start_floorlevel']                             = 'start_floorlevel'
        pipe_properties['end_floorlevel']                               = 'end_floorlevel'
        #%% Inspection properties (variables name directly from the dataset):
        inspection_properties['inspection_id']                          = 'sewer_inspection_id'
        inspection_properties['inspection_year']                        = 'inspection_year'
        inspection_properties['end_position']                           = 'end_position'
        inspection_properties['damage_length']                          = 'damage_length'
        inspection_properties['damage_code']                            = 'code'
        inspection_properties['damage_class']                           = 'damage_class'
        inspection_properties['position']                               = 'position'
        #%% Additional variables created in postprocessing (these variables are not part of the original dataset)
        add_var_inspection_properties['num_inspection_points']          = 'num_inspection_points'
        add_var_inspection_properties['pipe_age_during_inspection']     = 'pipe_age'
        
        varnames['pipe_properties']               = pipe_properties
        varnames['inspection_properties']         = inspection_properties
        varnames['add_var_inspection_properties'] = add_var_inspection_properties

    
    return varnames